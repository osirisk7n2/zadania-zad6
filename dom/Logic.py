__author__ = 'Ryujin'
import os
import datetime
import sqlite3

class database():
    sql_database='blogData.db'

    def ExecuteU(self,command):
        conn= sqlite3.connect(self.sql_database)
        cursor= conn.cursor()
        cursor.execute(command)
        conn.commit()
        conn.close()

    def CreateDb(self):
        if not (os.path.exists(self.sql_database)):
            print "Need to create db!"
            self.ExecuteU("""create table entries (id integer primary key autoincrement,
            date text, author text, content text)""")
            self.ExecuteU("""create table users (id integer primary key autoincrement,
            login text, password text)""")
        else:
            print "DB exists!"

    def AuthenticateUser(self,login,password):
        conn=sqlite3.connect(self.sql_database)
        cursor=conn.cursor()
        t=(login,password,)
        cursor.execute("select * from users where login=? and password=?",t)
        if cursor.fetchone() is None:
            conn.close()
            print "no entires"
            return False
        else:
            print cursor.fetchone()
            print "user found!"
            conn.close()
            return True


    def getAllEntries(self):
        conn=sqlite3.connect(self.sql_database)
        cursor= conn.cursor()
        cursor.execute("select * from entries")
        return cursor.fetchall()

    def addEntry(self,author,content):
        t=(datetime.datetime.today(),author,content,)
        self.ExecuteU('insert into entries (date,author,content) values (?,?,?)',t)


    def addUser(self,username,password):
        t=(username,password,)
        self.ExecuteU('insert into users (login,password) values(?,?)',t)

    def deleteEntry(self,id):
        conn=sqlite3.connect(self.sql_database)
        cursor=conn.cursor()
        t=(id,)
        print t
        cursor.execute('delete from entries where id=?',t)
        conn.commit()
        conn.close()

    def deleteUser(self,id):
        conn=sqlite3.connect(self.sql_database)
        cursor=conn.cursor()
        t=(id,)
        print t
        cursor.execute('delete from users where id=?',t)
        conn.commit()
        conn.close()

    def getAllUsers(self):
        conn=sqlite3.connect(self.sql_database)
        cursor= conn.cursor()
        cursor.execute("select * from users")
        return cursor.fetchall()