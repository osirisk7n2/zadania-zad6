__author__ = 'Ryujin'
from bottle import route,post, get, request
import sqlite3
import os
import Logic
import bottle
from bottle import jinja2_view as view
from bottle import  jinja2_template as template
from beaker.middleware import SessionMiddleware
from bottle import  static_file

session_opts = {
    'session.type': 'file',
    'session.cookie_expires': 300,
    'session.data_dir': './data',
    'session.auto': True
}
app = SessionMiddleware(bottle.app(), session_opts)



operacjeBazy = Logic.database()
operacjeBazy.CreateDb()



@route('/static/:filename#.*#')
def server_static(filename):
    return static_file(filename, root='./static/')


@route('/')
def index():
    session = request.environ['beaker.session']
    zalogowany = 'loged' in session
    author = 'username' in session
    if author:
        author=session['username']
    print zalogowany
    entries =operacjeBazy.getAllEntries()
    print entries
    return template("basic", logged=zalogowany, entries=reversed(entries),author=author)
    #return template("basic_template")

@route('/logout')
def logout():
    session = request.environ['beaker.session']
    session.delete()
    return "Wylogowano <br> <a href=\"/\">Powrot do strony glownej</a> "



@get('/login') # or @route('/login')
def login_form():
    return '''<div style=" text-align=center"><form method="POST" action="/login">
                <input name="name"     type="text" />
                <input name="password" type="password" />
                <input type="submit" />
              </form></div>'''


@post('/login') # or @route('/login', method='POST')
def login_submit():
    session = request.environ['beaker.session']
    name = request.forms.get('name')
    password = request.forms.get('password')
    if operacjeBazy.AuthenticateUser(name, password):
        session['loged']=True
        session['username']=name
        session.save()
        return """<p>Zalogowano pomyslnie!</p><br> <a href=\"/\">Wroc do strony glownej</a>"""

    else:
        return "<p>Niepoprawne dane!</p><br> <a href=\"/\">Wroc do strony glownej</a>"


@post('/')
def addEntry():
    session = request.environ['beaker.session']
    content = request.forms.get('content')
    author = 'username' in session
    if author:
        author=session['username']
    zalogowany = 'loged' in session
    if zalogowany:
        zalogowany=session['username']
    operacjeBazy.addEntry(author, content)
    entries=operacjeBazy.getAllEntries()

    return "Pomyslnie dodano wpis! <br> <a href=\"/\">Powrot do strony glownej</a> "


@post('/delete/<entry_id>')
def deleteEntry(entry_id):
    print entry_id
    operacjeBazy.deleteEntry(entry_id)

    return "Pomyslnie usunieto wpis! <br> <a href=\"/\">Powrot do strony glownej</a> "


@post('/users/delete/<user_id>')
def deleteUser(user_id):
    print user_id
    operacjeBazy.deleteUser(user_id)

    return "Pomyslnie usunieto uzytkownika! <br> <a href=\"/\">Powrot do strony glownej</a> "


@route('/users')
def usersList():
    session = request.environ['beaker.session']
    zalogowany = 'loged' in session
    if zalogowany:
        zalogowany=session['username']
        users =operacjeBazy.getAllUsers()
    return template('administration',logged=zalogowany,users=users)

@post('/users')
def addUser():
    session = request.environ['beaker.session']
    username = request.forms.get('login')
    password = request.forms.get('password')
    author = 'username' in session
    if author:
        author=session['username']
    zalogowany = 'loged' in session
    if zalogowany:
        zalogowany=session['username']

    if username=="" or password=="":
        return "Nie mozesz podac pustych danych!!!!"
    else:
        operacjeBazy.addUser(username,password)

    return "Pomyslnie dodano uzytkownika! <br> <a href=\"/\">Powrot do strony glownej</a> "


bottle.run(app=app)