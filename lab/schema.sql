create table books (
id integer primary key autoincrement not null,
title text,
isbn text,
publisher text,
author text
);