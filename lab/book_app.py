# -*- coding: utf-8 -*-

from flask import Flask
from flask import render_template
import os
import sqlite3
import bookdb

app = Flask(__name__)

schema_filename = 'schema.sql'
sql_db = 'Mbooks.db'
db_is_new = not os.path.exists(sql_db)


conn = sqlite3.connect(sql_db)

if db_is_new:
    print 'Need to create schema'
    print 'Creating schema'
    with open(schema_filename, 'rt') as f:
        schema = f.read()
    conn.executescript(schema)
    conn.execute("""insert into books (title, isbn, publisher, author) values
                 ('CherryPy Essentials: Rapid Python Web Application Development'
                 ,'978-1904811848','Packt Publishing (March 31, 2007)','Sylvain Hellegouarch')""")
    conn.execute("""insert into books (title, isbn, publisher, author) values
     ('Python for Software Design: How to Think Like a Computer Scientist',
     '9478-855','ktos tam publishing','sylian cos')""")
    conn.execute("""insert into books (title, isbn, publisher, author) values
                 ('CherryPysdfsdf Essentials: Rapid##### Python Web Application Development'
                 ,'978-19048134238','Packt pppb (March 31, 2007)','Sylvain HHRRRRRR')""")
    conn.commit()
else:
    print 'Database exists, assume schema does, too.'

conn.close()

db = bookdb.BookDB()


@app.route('/')
def books():
    print "kl"
    #baza=bookdb.BookDB()
    with sqlite3.connect(sql_db) as conn:
        cursor = conn.cursor()
        cursor.execute("""
         select id, title from books
         """)
        print "sdf"
        data =cursor.fetchall()
        zup=[]
        for nnt in data:
            id,title = nnt
            zup.append(nnt)
        print zup
        return render_template("book_list.html",entries=zup)
        # Przekaż listę książek do szablonu "book_list.html"
        pass


@app.route('/book/<book_id>/')
def book(book_id):
    baza=bookdb.BookDB()
    with sqlite3.connect(sql_db) as conn:
        print "sdfsdf"
        cursor = conn.cursor()
        cursor.execute("""
         select title,isbn, publisher, author  from books where id == $s
         """)%(book_id)
        data =cursor.fetchall()
        print data
        zup=[]
        for nnt in data:
            title,isbn, publisher, author = nnt
            zup.append(nnt)
        print zup

        return render_template("book_detail.html",details = zup)
    # Przekaż szczegóły danej książki do szablonu "book_detail.html"
    pass



if __name__ == '__main__':
    app.run(debug=False)